
[spec]

; Format and options of this spec file:
options = "+spec3"

[info]

artists = "

   Many Public Domain flags from:
    Sodipodi http://www.sodipodi.com/index.php3?section=clipart/flags
    Open Clip Art Library http://www.openclipart.org/

   Aborigines		Brett Roper
   Afghanistan		Andrew Duhan
   Algeria 		Lauris Kaplinski
   Antarctica		Tobias Jakobs
   Arab			Daniel Markstedt and Andrew Duhan
   Aram			Daniel Markstedt
   Argentina		Sigge Kotliar
   Armenia		Jason Short
   Assyria		Clevelander and Daniel Markstedt
   Australia		Daniel McRae
   Austria		Christoph Breitler
   Azerbaijan		Daniel McRae
   Aztec		Daniel Markstedt
   Babylon		Daniel Markstedt
   Bangladesh		Cezary Biele
   Barbarian		Tobias Jakobs
   Bavarian		Jörgen Scheibengruber
   Belgium		Daniel McRae
   Boer			Caleb Moore
   Bosnia		Daniel McRae
   Brasil		Cezary Biele
   Bulgaria		Daniel McRae
   Byzantium		Dragases
   Canada		James Leigh
   Cartago		Daniel Markstedt and Matt Jecrell
   Catalan		Xavier Conde Rueda
   Cheyenne		Arturo Espinosa-Aldama
   Chile		Mario Fuentes
   China		Stephen Silver
   Colombia		Lauris Kaplinski
   Constantine		Nikos Mavrogiannopoulos
   Cornwall		Vasco Alexandre Da Silva Costa
   Croatia		Frank Zeko
   Cuba			Lauris Kaplinski
   Czech		Lauris Kaplinski
   Denmark		Jens Bech Madsen
   Egypt (ancient)	Daniel Markstedt
   Egypt		Caleb Moore
   England		Thom Sanders
   Estonia		Daniel McRae
   Ethiopia		Lauris Kaplinski and Christian Schaller
   Europe		Lauris Kaplinski
   Finland		Daniel McRae
   France (old)		Patricia Fidi
   France		Tobias Jakobs
   Galicia		Miguel Rodríguez
   Gaul			Daniel Markstedt and Kilian Valkhof
   Georgia		Christian Schaller
   Germany		Philipp Sadleder
   Greece (ancient)	Daniel Markstedt and Vzb83 (Wikipedia)
   Greece		Daniel McRae
   Greenland		Daniel McRae
   Hittite		Daniel Markstedt
   Hungary		Laszlo Dvornik
   Iceland		Áki G. Karlsson
   Illyria		William Allen Simpson
   Inca			Daniel Markstedt
   India		Lauris Kaplinski
   Indonesia		Kuswanto
   Iran (ancient)	Daniel Markstedt
   Iran			Cezary Biele
   Iraq (old)		AnonMoos
   Iraq			Lauris Kaplinski
   Ireland		Lauris Kaplinski
   Iroquois		Daniel Markstedt
   Israel		John C Meuser
   Italy		Lauris Kaplinski
   Japan		Lauris Kaplinski
   Kampuchea		Caleb Moore
   Kenya		James Ots
   Korea		Stephen Silver
   Latvia		Lauris Bukšis
   Libya		Cezary Biele
   Lithuania		Christian Schaller
   Macedonia		Sigge Kotliar
   Madagascar		Russell Cloran
   Malaysia		Lauris Kaplinski
   Mali			Brett Roper
   Maori		Daniel Markstedt
   Mars			Vasco Alexandre Da Silva Costa
   Maya			Daniel Markstedt
   Mexico		Cezary Biele
   Mongolia		Sebastian Koppehel
   Myanmar		Caleb Moore
   NATO			Christian Schaller
   Netherlands		Marc Maurer
   New Zealand		Daniel McRae
   Nigeria		Brett Roper
   Norway		Christian Schaller
   Observer		Jason Short
   Ottoman		Daniel Markstedt
   Pakistan		Lauris Kaplinski
   Palmyra		JB Grout
   Papua New Guinea	Tobias Jakobs
   Peru			Brett Roper
   Philippines		Lauris Kaplinski
   Phoenicia		Vasco Alexandre Da Silva Costa
   Poland		Cezary Biele
   Portugal		Diogo Mamede
   Qing			Caleb Moore
   Quebec		Patrick Guimond
   Romania		Daniel McRae
   Rome			Vasco Alexandre Da Silva Costa, Patricia Fidi
   Russia		Oleg Krivosheev
   Rwanda		Cezary Biele
   Ryukyu		Daniel Markstedt
   Sapmi		Dean Tersigni
   Scotland		Sigge Kotliar
   Serbia		Nikola Pizurica
   Silesia		Vasco Alexandre Da Silva Costa
   Singapore		Lauris Kaplinski
   Slovakia		Lauris Kaplinski
   Slovenia		Matthew Gatto
   South Africa		Farrel Lifson
   Southern Cross	Crotalus horridus
   Soviet		Sigge Kotliar
   Spain		Pedro A. Gracia Fajardo
   Sri Lanka		Tobias Jakobs
   St. Patrick		Vasco Alexandre Da Silva Costa
   Sudan		Tobias Jakobs
   Sumeria		Franz Mach, Jason Short, Daniel Markstedt
   Swaziland		Caleb Moore
   Sweden		Richard Torkar
   Swiss		Philipp Frauenfelder
   Syria		Craig Kacinko
   Taiwan		Cezary Biele
   Texas		Paul B. Joiner
   Thailand		Lauris Kaplinski
   Tibet		Tobias Jakobs
   Tunisia		Tobias Jakobs
   Turkey		David Benbennick
   Ukraine		Lauris Kaplinski
   United Kingdom	Daniel McRae
   United Nations	Tobias Jakobs
   Unknown		Jason Short
   USA			Daniel McRae
   Uyghur		Daniel Markstedt
   Venezuela		Zach Harden et.al.
   Vietnam		Lauris Kaplinski
   Viking		Daniel Markstedt
   Wales		Tobias Jakobs
   Westphalia		Daniel Markstedt and Tobias Jakobs
   Zulu			Daniel Markstedt and Caleb Moore

"
[extra]
sprites =
	{	"tag", "file"
		"f.aborigines", "tinydent/flags/aborigines"
		"f.afghanistan", "tinydent/flags/afghanistan"
		"f.algeria", "tinydent/flags/algeria"
		"f.antarctica", "tinydent/flags/antarctica"
		"f.arab", "tinydent/flags/arab"
		"f.aram", "tinydent/flags/aram"
		"f.argentina", "tinydent/flags/argentina"
		"f.armenia", "tinydent/flags/armenia"
		"f.assyria", "tinydent/flags/assyria"
		"f.australia", "tinydent/flags/australia"
		"f.austria", "tinydent/flags/austria"
		"f.azerbaijan", "tinydent/flags/azerbaijan"
		"f.aztec", "tinydent/flags/aztec"
		"f.babylon", "tinydent/flags/babylon"
		"f.bangladesh", "tinydent/flags/bangladesh"
		"f.barbarian", "tinydent/flags/barbarian"
		"f.bavarian", "tinydent/flags/bavarian"
		"f.belgium", "tinydent/flags/belgium"
		"f.boer", "tinydent/flags/boer"			; old south african
		"f.bosnia", "tinydent/flags/bosnia"
		"f.brasil", "tinydent/flags/brasil"
		"f.bulgaria", "tinydent/flags/bulgaria"
		"f.byzantium", "tinydent/flags/byzantium"
		"f.canada", "tinydent/flags/canada"
		"f.cartago", "tinydent/flags/cartago"
		"f.catalan", "tinydent/flags/catalan"
		"f.cheyenne", "tinydent/flags/cheyenne"		; used by Sioux
		"f.chile", "tinydent/flags/chile"
		"f.china", "tinydent/flags/china"
		"f.colombia", "tinydent/flags/colombia"
		"f.columbia", "tinydent/flags/colombia"	; For backward compatibility
		"f.constantine", "tinydent/flags/constantine"	; alt. Byzantine
		"f.cornwall", "tinydent/flags/cornwall"
		"f.croatia", "tinydent/flags/croatia"
		"f.cuba", "tinydent/flags/cuba"
		"f.czech", "tinydent/flags/czech"
		"f.denmark", "tinydent/flags/denmark"
		"f.egypt_ancient", "tinydent/flags/egypt_ancient"
		"f.egypt", "tinydent/flags/egypt"
		"f.england", "tinydent/flags/england"
		"f.estonia", "tinydent/flags/estonia"
		"f.ethiopia", "tinydent/flags/ethiopia"
		"f.europe", "tinydent/flags/europe"
		"f.finland", "tinydent/flags/finland"
		"f.france_old", "tinydent/flags/france_old"	; alt Gallic
		"f.france", "tinydent/flags/france"
		"f.galicia", "tinydent/flags/galicia"
		"f.gaul", "tinydent/flags/gaul"
		"f.georgia", "tinydent/flags/georgia"
		"f.germany", "tinydent/flags/germany"
		"f.greece_ancient", "tinydent/flags/greece_ancient"
		"f.greece", "tinydent/flags/greece"
		"f.greenland", "tinydent/flags/greenland"	; used by Inuit
		"f.hittite", "tinydent/flags/hittite"
		"f.holland", "tinydent/flags/netherlands"	; backward compatabiliy
		"f.hungary", "tinydent/flags/hungary"
		"f.iceland", "tinydent/flags/iceland"
		"f.illyria", "tinydent/flags/illyria"
		"f.india", "tinydent/flags/india"
		"f.inca", "tinydent/flags/inca"
		"f.indonesia", "tinydent/flags/indonesia"
		"f.iran_ancient", "tinydent/flags/iran_ancient"
		"f.iran", "tinydent/flags/iran"
		"f.iraq_old", "tinydent/flags/iraq_old"		; used by Babylonian
		"f.iraq", "tinydent/flags/iraq"
		"f.ireland", "tinydent/flags/ireland"
		"f.iroquois", "tinydent/flags/iroquois"
		"f.israel", "tinydent/flags/israel"
		"f.italy", "tinydent/flags/italy"
		"f.japan", "tinydent/flags/japan"
		"f.kampuchea", "tinydent/flags/kampuchea"
		"f.kenya", "tinydent/flags/kenya"
		"f.korea", "tinydent/flags/korea"
		"f.latvia", "tinydent/flags/latvia"
		"f.libya", "tinydent/flags/libya"
		"f.lithuania", "tinydent/flags/lithuania"
		"f.macedonia", "tinydent/flags/macedonia"
		"f.madagascar", "tinydent/flags/madagascar"
		"f.malaysia", "tinydent/flags/malaysia"
		"f.mali", "tinydent/flags/mali"
		"f.maori", "tinydent/flags/maori"
		"f.mars", "tinydent/flags/mars"
		"f.maya", "tinydent/flags/maya"
		"f.mexico", "tinydent/flags/mexico"
		"f.mongolia", "tinydent/flags/mongolia"
		"f.myanmar", "tinydent/flags/myanmar"
		"f.nato", "tinydent/flags/nato"
		"f.netherlands", "tinydent/flags/netherlands"
		"f.newzealand", "tinydent/flags/newzealand"
		"f.nigeria", "tinydent/flags/nigeria"
		"f.norway", "tinydent/flags/norway"
		"f.observer", "tinydent/flags/observer"
 		"f.ottoman", "tinydent/flags/ottoman"
		"f.papua_newguinea", "tinydent/flags/papua_newguinea"
		"f.pakistan", "tinydent/flags/pakistan"
		"f.palmyra", "tinydent/flags/palmyra"
		"f.peru", "tinydent/flags/peru"
		"f.philippines", "tinydent/flags/philippines"
		"f.phoenicia", "tinydent/flags/phoenicia"
		"f.poland", "tinydent/flags/poland"
		"f.portugal", "tinydent/flags/portugal"
		"f.qing", "tinydent/flags/qing"			; Manchu empire flag
		"f.quebec", "tinydent/flags/quebec"
		"f.romania", "tinydent/flags/romania"
		"f.rome", "tinydent/flags/rome"			; Roman republic flag
		"f.russia", "tinydent/flags/russia"
		"f.rwanda", "tinydent/flags/rwanda"		; Alternate Zulu
		"f.ryukyu", "tinydent/flags/ryukyu"
		"f.sapmi", "tinydent/flags/sapmi"
		"f.scotland", "tinydent/flags/scotland"
		"f.serbia", "tinydent/flags/serbia"
		"f.silesia", "tinydent/flags/silesia"
		"f.singapore", "tinydent/flags/singapore"
		"f.slovakia", "tinydent/flags/slovakia"
		"f.slovenia", "tinydent/flags/slovenia"
		"f.south_africa", "tinydent/flags/south_africa"
		"f.southern_cross", "tinydent/flags/southern_cross" ; confederate battle flag
		"f.soviet", "tinydent/flags/soviet"
		"f.spain", "tinydent/flags/spain"
		"f.srilanka", "tinydent/flags/srilanka"
		"f.stpatrick", "tinydent/flags/stpatrick"	; Alternate Irish 
		"f.sudan", "tinydent/flags/sudan"
		"f.sumeria", "tinydent/flags/sumeria"
		"f.swaziland", "tinydent/flags/swaziland"
		"f.sweden", "tinydent/flags/sweden"
		"f.switzerland", "tinydent/flags/swiss"
		"f.syria", "tinydent/flags/syria"
		"f.taiwan", "tinydent/flags/taiwan"
		"f.texas", "tinydent/flags/texas"
		"f.thailand", "tinydent/flags/thailand"
		"f.tibet", "tinydent/flags/tibet"
		"f.tunisia", "tinydent/flags/tunisia"
		"f.turkey", "tinydent/flags/turkey"
		"f.ukraine", "tinydent/flags/ukraine"
		"f.united_kingdom", "tinydent/flags/united_kingdom"
		"f.united_nations", "tinydent/flags/united_nations"
		"f.unknown", "tinydent/flags/unknown"		; hard-coded fallback
		"f.usa", "tinydent/flags/usa"
		"f.uyghur", "tinydent/flags/uyghur"
		"f.venezuela", "tinydent/flags/venezuela"
		"f.vietnam", "tinydent/flags/vietnam"
		"f.viking", "tinydent/flags/viking"
		"f.wales", "tinydent/flags/wales"
		"f.westphalia", "tinydent/flags/westphalia"
		"f.zulu", "tinydent/flags/zulu"
	}
